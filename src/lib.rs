use std::fs;
use std::path::Path;

use futures::future::join_all;
use log::{info, warn};
use serde::Deserialize;
use serde_json::Value;
use tide::http::Method;
use tide::{Request, Response};

pub async fn spawn_server(path: &Path) {
    let mut servers = Vec::new();

    for s in load_servers(path) {
        let mut server = tide::new();

        for endpoint in s.endpoints {
            let route = &mut server.at(&endpoint.path);

            match endpoint.method.parse::<Method>() {
                Ok(method) => {
                    route.method(method, move |request| {
                        log_http_request(
                            request,
                            endpoint.status_code,
                            endpoint.headers.clone(),
                            endpoint.body.clone().map(|body| body.into_bytes()),
                        )
                    });
                }
                Err(e) => warn!("Ignoring unknown method: {}", e),
            }
        }

        servers.push(server.listen(format!("127.0.0.1:{}", s.port)));
    }

    join_all(servers).await;
}

fn load_servers(path: &Path) -> Vec<Server> {
    let file_content = fs::read_to_string(path).expect("Failed to read file");
    serde_yaml::from_str(&file_content).expect("Invalid YAML provided")
}

pub async fn log_http_request(
    mut request: Request<()>,
    status_code: u16,
    response_headers: Option<Vec<Header>>,
    response_body: Option<Vec<u8>>,
) -> tide::Result {
    info!("URL: {:<20}", request.url());

    // sort headers alphabetically
    let mut request_headers = request.iter().collect::<Vec<_>>();
    request_headers.sort_by(|a, b| ((a.0).to_string()).cmp(&(b.0).to_string()));

    // print headers
    request_headers
        .iter()
        .for_each(|(name, value)| info!("Header: {:<20}{:<50}", name.to_string(), value.as_str()));

    // pretty print if body is valid JSON
    if let Ok(body) = request.body_string().await {
        if !body.is_empty() {
            if let Ok(json) = serde_json::from_str(&body) {
                info!(
                    "Body: {}",
                    serde_json::to_string_pretty::<Value>(&json).unwrap()
                );
            } else {
                info!("Body: {:<20}", body)
            }
        }
    }

    // build HTTP response
    let mut response = Response::new(status_code);

    if let Some(body) = response_body {
        response.set_body(body);
        response.remove_header("Content-Type"); // Content-Type header is set automatically
    }

    if let Some(headers) = response_headers {
        for h in headers {
            response.append_header(&*h.key, &*h.value);
        }
    }

    Ok(response)
}

#[derive(Deserialize)]
pub struct Server {
    port: u32,
    endpoints: Vec<Endpoint>,
}

#[derive(Deserialize, Clone)]
pub struct Endpoint {
    path: String,
    method: String,
    status_code: u16,
    headers: Option<Vec<Header>>,
    body: Option<String>,
}

#[derive(Deserialize, Clone)]
pub struct Header {
    key: String,
    value: String,
}

impl Header {
    pub fn new(key: String, value: String) -> Self {
        Header { key, value }
    }
}
