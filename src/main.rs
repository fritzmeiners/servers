#![warn(
    future_incompatible,
    nonstandard_style,
    rustdoc::all,
    rust_2018_idioms,
    unused
)]

use std::path::Path;

use clap::{Arg, Command};
use futures::future::join_all;
use futures::io::ErrorKind;
use log::{error, info, warn};

use servers::{log_http_request, spawn_server, Header};

static BANNER: &str = include_str!("../resources/banner.txt");

#[async_std::main]
async fn main() -> Result<(), std::io::Error> {
    let arg_matches = Command::new("Mock Server")
        .version("0.4.0")
        .about("Spawns a mock server for testing purposes")
        .arg(
            Arg::new("port")
                .short('p')
                .long("port")
                .takes_value(true)
                .multiple(true)
                .validator(is_valid_port)
                .conflicts_with("config")
                .help("Specify the port the server will listen on"),
        )
        .arg(
            Arg::new("status_code")
                .short('s')
                .long("status")
                .takes_value(true)
                .validator(is_valid_status_code)
                .conflicts_with("config")
                .help("Specify the status code to return"),
        )
        .arg(
            Arg::new("header")
                .short('h')
                .long("header")
                .takes_value(true)
                .multiple(true)
                .validator(is_valid_header)
                .help("Header to add to response"),
        )
        .arg(
            Arg::new("data")
                .short('d')
                .long("data")
                .takes_value(true)
                .multiple(false)
                .help("Payload to add to response"),
        )
        .arg(
            Arg::new("config")
                .short('c')
                .long("config")
                .takes_value(true)
                .conflicts_with_all(&["port", "status_code", "header"])
                .help("Sets the configuration file"),
        )
        .get_matches();

    log4rs::init_file("resources/log4rs.yml", Default::default()).unwrap();
    info!("\n\n{}\n\n", BANNER);

    // config file overrides other args
    if let Some(config) = arg_matches.value_of("config") {
        let server_config = Path::new(config);

        if server_config.exists() {
            spawn_server(server_config).await;
            Ok(())
        } else {
            Err(std::io::Error::new(
                ErrorKind::NotFound,
                "No such file or directory",
            ))
        }
    } else {
        let status_code = arg_matches
            .value_of("status_code")
            .unwrap_or("200")
            .parse::<u16>()
            .unwrap();

        // using a default value in arg specification clashes w/ the 'conflicts_with' requirement
        let addresses = if arg_matches.is_present("port") {
            arg_matches
                .values_of("port")
                .unwrap()
                .map(|port| format!("127.0.0.1:{}", port))
                .collect()
        } else {
            vec!["127.0.0.1:8080".to_owned()]
        };

        let headers: Option<Vec<Header>> = if arg_matches.is_present("header") {
            Some(
                arg_matches
                    .values_of("header")
                    .unwrap()
                    .map(|h| {
                        let mut key_value = h.split(':');
                        Header::new(
                            key_value.next().unwrap().to_string(),
                            key_value.next().unwrap().to_string(),
                        )
                    })
                    .collect(),
            )
        } else {
            None
        };

        // PathBuf::from(arg_matches.value_of("data").unwrap().to_string())
        let data: Option<String> = if arg_matches.is_present("data") {
            Some(arg_matches.value_of("data").unwrap().to_string())
        } else {
            None
        };

        join_all(addresses.iter().map(|address| {
            build_server(status_code, headers.clone(), data.clone()).listen(address)
        }))
        .await;
        Ok(())
    }
}

fn build_server(
    status_code: u16,
    headers: Option<Vec<Header>>,
    data: Option<String>,
) -> tide::Server<()> {
    let mut server = tide::new();

    // listen to all incoming requests, regardless of their method
    // bug in routing requires duplicates: https://github.com/http-rs/route-recognizer/issues/45
    let payload = if let Some(data_input) = data {
        if data_input.starts_with('@') {
            let path = &data_input[1..data_input.len()];

            match std::fs::read(path) {
                Ok(file_contents) => Some(file_contents),
                Err(why) => {
                    error!(
                        "Failed to read file [{}], responding with empty body: {}",
                        path, why
                    );
                    None
                }
            }
        } else {
            Some(data_input.into_bytes())
        }
    } else {
        None
    };

    let response_headers = headers.clone();
    let response_payload = payload.clone();

    server.at("/").all(move |request| {
        log_http_request(request, status_code, headers.clone(), payload.clone())
    });
    server.at("*").all(move |request| {
        log_http_request(
            request,
            status_code,
            response_headers.clone(),
            response_payload.clone(),
        )
    });

    server
}

fn is_valid_port(port: &str) -> Result<(), String> {
    match port.parse::<u16>() {
        Ok(_) => Ok(()),
        _ => Err(String::from("Port number must be between 0 and 65,535")),
    }
}

fn is_valid_status_code(status_code: &str) -> Result<(), String> {
    let error_message: String = String::from("Status code must be in range between 200 and 511");

    match status_code.parse::<u16>() {
        Ok(code) => {
            if (200..=511).contains(&code) {
                Ok(())
            } else {
                Err(error_message)
            }
        }
        _ => Err(error_message),
    }
}

fn is_valid_header(header: &str) -> Result<(), String> {
    match header.contains(':') && header.chars().all(|c| char::is_ascii(&c)) {
        true => Ok(()),
        _ => Err(String::from(
            "Header must only contain ASCII and contain a colon",
        )),
    }
}
